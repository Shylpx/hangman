# Game arena

## Front end

### Dependencies

- Install node.js 16.19.0, npm 7.19.0 and yarn 1.22.5
```
curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs
```
```
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
```

- Install Vue CLI
```
yarn global add @vue/cli
```

### Create project (for the first time)

- Create project including the following features: `babel`, `Router`, `Vuex`, and `Linter / Formatter` (`ESLint + Prettier`)
```
> vue create game-arena
> cd game-arena
```

- Install vuetify with the default preset
```
vue add vuetify
```

- Install firebase
```
yarn add firebase
```

- Add `pug` as template engine and `Sass/SCSS` as style compiler.
```
yarn add pug pug-plain-loader
yarn add sass-loader node-sass
```

### Commands

- Project setup
```
yarn install -std=c++17
```

- Compiles and hot-reloads for development
```
yarn serve --port 3001
```

- Compiles and minifies for production
```
yarn build
```

- Lints and fixes files
```
yarn lint
```

### Environment variables

Environment variables must be in the `.env` (production) or `.env.development`, according to the environment. The following environment variables are required:

- Firebase configuration:
```
VUE_APP_FIREBASE_API_KEY="?"
VUE_APP_FIREBASE_AUTH_DOMAIN="?"
VUE_APP_FIREBASE_DATABASE_URL="?"
VUE_APP_FIREBASE_PROJECT_ID="?"
VUE_APP_FIREBASE_STORAGE_BUCKET="?"
VUE_APP_FIREBASE_MESSAGING_SENDER_ID="?"
VUE_APP_FIREBASE_APP_ID="?"
```

## Back end

### Configure Firebase

- Install Firebase CLI
```
curl -sL https://firebase.tools | bash
```

- Firebase login
```
firebase login
```

- Initialize Firebase (for the first time), selecting an existent project or creating a new one, and enabling the following services: Cloud Functions, Realtime Database and Firestore.
```
firebase init functions
firebase init database
firebase init firestore
firebase init hosting
```

### Commands

- Firebase login
```
firebase login
```

- Start emulators (`functions`, `database` and `firestore`) importing initial data
```
firebase emulators:start --import ./emulators_data
```
> Realtime database service requires the `GOOGLE_APPLICATION_CREDENTIALS` environment variable, with the path to the project service account's private key.

```
export GOOGLE_APPLICATION_CREDENTIALS="path/to/google-key.json"
```

- Export emulators data
```
firebase emulators:export ./emulators_data
```

- Deploy functions
```
firebase deploy --only functions
```

- Deploy hosting
```
firebase deploy --only hosting
```

- Deploy rules
```
firebase deploy --only firestore:rules
```

- Disable hosting
```
firebase hosting:disable
```

- Run linter
```
npm --prefix ./functions run lint
```

### Deploy to production

```
yarn build
firebase deploy --only hosting

firebase deploy --only functions

firebase deploy --only firestore:rules
```
