const functions = require("firebase-functions");
const admin = require("firebase-admin");
const { Timestamp, FieldValue } = require("firebase-admin/firestore");
const { getFunctions } = require("firebase-admin/functions");
const md5 = require("md5");
const axios = require("axios");
const turns = require("./turns");

const ROOM_CODE_LENGTH = 4;
const ROOM_DELAY_SECONDS = 5;
const GAME_DEFAULTS = {
  maxFailures: 10,
  turnSeconds: 40
};
const HIDDEN_CHAR = "_";
const MAX_TERM_WEIGHT = 100;

exports.createRoom = functions.https.onCall(async (data, context) => {
  const room = await generateRoom(data.game, [(await getUserData(context.auth)) || data.guest]);
  return { room: { uid: room.id, ...(await room.get()).data() } };
});

exports.createRoomFromGame = functions.https.onCall(async (data, _context) => {
  const doc = await admin
    .firestore()
    .collection("games")
    .doc(data.gameUid)
    .get();
  const room = await generateRoom(
    doc.data().type,
    Object.entries(doc.data().player_cards)
      .map(([uid, data]) => ({ uid, ...data }))
      .sort((player1, player2) => player1.position - player2.position)
  );
  return { room: { uid: room.id, ...(await room.get()).data() } };
});

exports.joinRoom = functions.https.onCall(async (data, context) => {
  const user = (await getUserData(context.auth)) || data.guest;
  const room = await getRoom(data.roomCode);
  if (!room || room.data().state !== "open") return { room: null };

  await addRoomMember(user, room.id);
  return { room: { uid: room.id, ...(await getRoom(data.roomCode)).data() } };
});

exports.leaveRoom = functions.https.onCall(async (data, context) => {
  const room = await getRoom(data.roomCode);
  if (!room || room.data().state !== "open") return { room: null };

  await removeRoomMember((context.auth || data.guest).uid, room.id);
  return { room: (await getRoom(data.roomCode)).data() };
});

exports.checkRoomState = functions.https.onCall(async (data, _context) => {
  return { room: (await getRoom(data.roomCode)).data() };
});

exports.startGame = functions.https.onCall(async (data, _context) => {
  // TODO: Avoid creating games with only 1 player
  const room = await getRoom(data.roomCode);
  const gameStartDate = generateGameStartDate();

  setReadyState(room.id, gameStartDate);

  var roomData = room.data();
  roomData.state = "ready";
  roomData.game_starts_at = gameStartDate;

  if (process.env.EMULATOR === "true") {
    setTimeout(async () => {
      const url =
        `http://127.0.0.1:5001/${process.env.PROJECT_ID}/${process.env.REGION}` +
        "/rooms-generateGame";
      await axios.default({
        method: "POST",
        url: url,
        data: { data: { roomUid: room.id, roomData, settings: data.settings } }
      });
    }, ROOM_DELAY_SECONDS * 1000);
  } else {
    getFunctions()
      .taskQueue("rooms-generateGame")
      .enqueue(
        { roomUid: room.id, roomData, settings: data.settings },
        { scheduleTime: gameStartDate.toDate() }
      );
  }
});

exports.generateGame = functions.tasks
  .taskQueue({
    retryConfig: { maxAttempts: 1 },
    rateLimits: { maxConcurrentDispatches: 5 }
  })
  .onDispatch(async (data, _context) => {
    // TODO: Manage language setting
    const language = "es";
    const string = await chooseRandomTerm(data.settings.termLists, language);
    admin
      .firestore()
      .collection("term_log")
      .doc(getTermHash(string, language))
      .set({ last_time_played: Timestamp.fromDate(new Date()) });

    const playerList = shuffle(data.roomData.member_uids);
    const turnSeconds = GAME_DEFAULTS.turnSeconds;

    admin
      .firestore()
      .collection("games")
      .add({
        type: data.roomData.game,
        state: "playing",
        max_failures: GAME_DEFAULTS.maxFailures,
        turn_seconds: turnSeconds,
        string: string,
        current_player: playerList[0],
        last_turn_started_at: Timestamp.fromDate(new Date()),
        skipped_action_count: 0,
        player_cards: Object.fromEntries(
          new Map(
            playerList.map((member, index) => {
              return [
                member,
                {
                  name: data.roomData.member_data[member].name,
                  beacon: true,
                  failures: 0,
                  string: hideLetters(string),
                  selected_keys: [],
                  index: index,
                  position: null
                }
              ];
            })
          )
        )
      })
      .then(docRef => {
        admin
          .firestore()
          .collection("rooms")
          .doc(data.roomUid)
          .update({ state: "game" });

        turns.scheduleNextTurn(docRef.id, turnSeconds);

        return;
      })
      .catch(() => {
        return;
      });
  });

async function getUserData(auth) {
  if (!auth) return null;

  const user = await admin
    .firestore()
    .collection("users")
    .doc(auth.uid)
    .get();

  return { uid: auth.uid, ...user.data() };
}

async function getRoom(roomCode) {
  const snapshot = await admin
    .firestore()
    .collection("rooms")
    .where("code", "==", roomCode)
    .get();
  return snapshot.docs[0];
}

async function generateRoom(game, users) {
  do {
    var roomCode = generateRandomRoomCode();
    var existentRoom = await getRoom(roomCode); // eslint-disable-line no-await-in-loop
  } while (existentRoom !== undefined);

  return admin
    .firestore()
    .collection("rooms")
    .add({
      game,
      code: roomCode,
      state: "open",
      member_uids: users.map(user => user.uid),
      member_data: Object.fromEntries(
        users.map(user => {
          return [
            user.uid,
            {
              name: user.name,
              avatar_hash: md5(user.email || Math.random())
            }
          ];
        })
      ),
      created_at: Timestamp.fromDate(new Date()),
      game_starts_at: null
    });
}

function addRoomMember(user, roomUid) {
  return admin
    .firestore()
    .collection("rooms")
    .doc(roomUid)
    .update({
      member_uids: FieldValue.arrayUnion(user.uid),
      [`member_data.${user.uid}`]: {
        name: user.name,
        avatar_hash: md5(user.email || Math.random())
      }
    });
}

function removeRoomMember(userUid, roomUid) {
  return admin
    .firestore()
    .collection("rooms")
    .doc(roomUid)
    .update({
      member_uids: FieldValue.arrayRemove(userUid),
      [`member_data.${userUid}`]: FieldValue.delete()
    });
}

function setReadyState(roomUid, gameStartDate) {
  admin
    .firestore()
    .collection("rooms")
    .doc(roomUid)
    .update({
      state: "ready",
      game_starts_at: gameStartDate
    });
}

function generateRandomRoomCode() {
  return Math.random()
    .toString(36)
    .substr(2, ROOM_CODE_LENGTH)
    .toUpperCase();
}

function generateGameStartDate() {
  return Timestamp.fromDate(new Date(new Date().getTime() + ROOM_DELAY_SECONDS * 1000));
}

function shuffle(list) {
  return list.sort(() => Math.random() - 0.5);
}

function hideLetters(string) {
  return [...string].map(l => (/\p{L}/u.test(l) ? HIDDEN_CHAR : l)).join("");
}

function getTermHash(term, language) {
  return md5(`${term}:${language}`);
}

function weightedRandom(list) {
  const cumulativeWeights = list.map((sum => element => (sum += element.weight))(0));
  const randomNumber =
    Math.floor(Math.random() * cumulativeWeights[cumulativeWeights.length - 1]) + 1;
  const termIndex = cumulativeWeights.findIndex(weight => weight >= randomNumber);
  return list[termIndex].term;
}

async function chooseRandomTerm(categories, language) {
  let selection = admin.firestore().collection("term_categories");
  if (categories && categories.length > 0) {
    const randomCategory = categories[Math.floor(Math.random() * categories.length)];
    selection = selection.where("category", "==", randomCategory);
  }
  selection = selection.where("language", "==", language);

  const categoryDocuments = (await selection.get()).docs;
  const terms = categoryDocuments[Math.floor(Math.random() * categoryDocuments.length)].data().list;

  const termLog = await admin
    .firestore()
    .collection("term_log")
    .where(
      "last_time_played",
      ">",
      Timestamp.fromMillis(new Date().setDate(new Date().getDate() - MAX_TERM_WEIGHT))
    )
    .get();
  const termWeights = termLog.docs.reduce((list, doc) => {
    list[doc.id] =
      Math.floor((new Date() - doc.data().last_time_played.toDate()) / (1000 * 60 * 60 * 24)) + 1;
    return list;
  }, {});

  return weightedRandom(
    terms.map(term => ({
      term: term,
      weight: termWeights[getTermHash(term, language)] || MAX_TERM_WEIGHT
    }))
  );
}
