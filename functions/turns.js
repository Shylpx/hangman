const functions = require("firebase-functions");
const admin = require("firebase-admin");
const { Timestamp, FieldValue } = require("firebase-admin/firestore");
const { getFunctions } = require("firebase-admin/functions");
const axios = require("axios");

const MAX_TURNS_WITHOUT_ACTIVITY = 3;

exports.changePlayer = functions.tasks
  .taskQueue({
    retryConfig: { maxAttempts: 1 },
    rateLimits: { maxConcurrentDispatches: 100 }
  })
  .onDispatch(async (data, _context) => {
    const game = await getGame(data.gameUid);

    if (data.triggeredAt < game.last_turn_started_at.valueOf()) {
      return;
    }

    // NOTE: Termination condition
    if (
      game.skipped_action_count / Object.keys(game.player_cards).length >=
      MAX_TURNS_WITHOUT_ACTIVITY
    ) {
      admin
        .firestore()
        .collection("games")
        .doc(data.gameUid)
        .update({ state: "expired" });
      return;
    }

    exports.assignNextPlayer(
      data.gameUid,
      game.turn_seconds,
      exports.calculateNextPlayer(game.current_player, game.player_cards)
    );
  });

exports.assignNextPlayer = function(gameUid, delaySeconds, nextPlayer, skippedAction = true) {
  admin
    .firestore()
    .collection("games")
    .doc(gameUid)
    .update({
      last_turn_started_at: now(),
      current_player: nextPlayer,
      skipped_action_count: skippedAction ? FieldValue.increment(1) : 0
    });

  exports.scheduleNextTurn(gameUid, delaySeconds);

  return;
};

exports.scheduleNextTurn = function(gameUid, delaySeconds) {
  const triggeredAt = now().valueOf();

  if (process.env.EMULATOR === "true") {
    setTimeout(async () => {
      const url =
        `http://127.0.0.1:5001/${process.env.PROJECT_ID}/${process.env.REGION}` +
        "/turns-changePlayer";
      await axios.default({
        method: "POST",
        url: url,
        data: { data: { triggeredAt: triggeredAt, gameUid } }
      });
    }, delaySeconds * 1000);
  } else {
    getFunctions()
      .taskQueue("turns-changePlayer")
      .enqueue(
        { triggeredAt: triggeredAt, gameUid },
        { scheduleTime: new Date(new Date().getTime() + delaySeconds * 1000) }
      );
  }
};

exports.calculateNextPlayer = function(currentPlayer, playerCards) {
  const playerList = sortedActivePlayers(playerCards);
  return playerList[(playerList.findIndex(p => currentPlayer === p) + 1) % playerList.length];
};

async function getGame(gameUid) {
  const doc = await admin
    .firestore()
    .collection("games")
    .doc(gameUid)
    .get();
  return doc.data();
}

function sortedActivePlayers(playerCards) {
  const activePlayerCards = Object.fromEntries(
    Object.entries(playerCards).filter(([_uid, player]) => !player.position)
  );
  return Object.keys(activePlayerCards).sort((a, b) => playerCards[a].index - playerCards[b].index);
}

function now() {
  return Timestamp.fromDate(new Date());
}
