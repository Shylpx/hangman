import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import * as firebase from "firebase";

import { v4 as uuidv4 } from "uuid";
Object.defineProperty(Vue.prototype, "$uuidv4", { value: uuidv4 });

import fakergem from "fakergem";
Object.defineProperty(Vue.prototype, "$fakergem", { value: fakergem });

import VueClipboard from "vue-clipboard2";
Vue.use(VueClipboard);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");

firebase.initializeApp({
  apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
  authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.VUE_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.VUE_APP_FIREBASE_APP_ID
});

firebase.auth().onAuthStateChanged(user => {
  store.dispatch("fetchUser", user || JSON.parse(localStorage.getItem("user") || "null"));
});

if (process.env.NODE_ENV === "development") {
  window.firebase = firebase; // NOTE: Firebase debug

  // NOTE: Connect Firebase to Cloud Functions and Firestore local emulators
  firebase.functions().useFunctionsEmulator(process.env.VUE_APP_FIREBASE_CLOUD_FUNCTIONS_URL);

  firebase.firestore().settings({ host: process.env.VUE_APP_FIREBASE_FIRESTORE_URL, ssl: false });
}
